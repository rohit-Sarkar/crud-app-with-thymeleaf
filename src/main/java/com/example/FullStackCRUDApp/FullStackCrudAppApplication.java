package com.example.FullStackCRUDApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FullStackCrudAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(FullStackCrudAppApplication.class, args);
	}

}
